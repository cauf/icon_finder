unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, StdCtrls,
  ExtCtrls, ShellCtrls, ComCtrls, SynEdit, SynHighlighterAny, FileUtil, about,
  Clipbrd;

type

  { TMainWindow }

  TMainWindow = class(TForm)
    BtnSearch: TButton;
    ChGFileTypes: TCheckGroup;
    ImlMain: TImageList;
    TxbWidth: TEdit;
    TxbHeight: TEdit;
    ImLResults: TImageList;
    TxbFileNamePattern: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LtvResults: TListView;
    MainMenu: TMainMenu;
    MIInfo: TMenuItem;
    MIAbout: TMenuItem;
    MainProgress: TProgressBar;
    STVFolderPicker: TShellTreeView;
    MainStatus: TStatusBar;
    procedure BtnSearchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LtvResultsClick(Sender: TObject);
    procedure MIAboutClick(Sender: TObject);
  private

  public

  end;

var
  MainWindow: TMainWindow;

implementation

const
  CRLF = #10#13;

{$R *.lfm}

{ private procedures }

function GetFilesMask(pattern: string): string;
begin
  if MainWindow.ChGFileTypes.Checked[0] then
    Result := '*' + pattern + '*.jpg;*' + pattern + '*.jpeg';
  if MainWindow.ChGFileTypes.Checked[1] then
  begin
    if length(Result) > 1 then
      Result := Result + ';';
    Result := Result + '*' + pattern + '*.bmp';
  end;
  if MainWindow.ChGFileTypes.Checked[2] then
  begin
    if length(Result) > 1 then
      Result := Result + ';';
    Result := Result + '*' + pattern + '*.ico';
  end;
  if MainWindow.ChGFileTypes.Checked[3] then
  begin
    if length(Result) > 1 then
      Result := Result + ';';
    Result := Result + '*' + pattern + '*.png';
  end;
end;

procedure SetStatus(Text: string);
begin
  MainWindow.MainStatus.Panels[1].Text := Text;
end;

{ TMainWindow }

procedure TMainWindow.FormCreate(Sender: TObject);
var
  homepath, homedrive: string;
begin
  homepath := GetEnvironmentVariable('HOMEPATH');
  homedrive := GetEnvironmentVariable('HOMEDRIVE');
  STVFolderPicker.Path := homedrive + homepath + PathDelim + 'Documents';
  LtvResults.SmallImages := ImLResults;
end;

procedure TMainWindow.LtvResultsClick(Sender: TObject);
var
  clipbrd: TClipboard;
begin
    clipbrd := TClipboard.Create;
    clipbrd.SetTextBuf(PChar(LtvResults.Selected.Caption));
    SetStatus('Значение пути к файлу скопировано в буфер обмена');
end;

procedure TMainWindow.MIAboutClick(Sender: TObject);
var
  aboutWnd: TAboutWindow;
begin
  aboutWnd := TAboutWindow.Create(MainWindow);
  aboutWnd.Show;
end;

procedure TMainWindow.BtnSearchClick(Sender: TObject);
var
  mask, pattern, path, resolution: string;
  files: TStringList;
  fi, ii, sWidth, sHeight: integer;
  pic: TPicture;
begin
  sWidth := StrToInt(TxbWidth.Text);
  sHeight := StrToInt(TxbHeight.Text);
  path := STVFolderPicker.Path;
  SetStatus('Поиск файлов в каталоге "' + path + '"...');
  pattern := TxbFileNamePattern.Text;
  // получение списка масок
  mask := GetFilesMask(pattern);
  // поиск файлов
  files := FindAllFiles(path, mask, True);
  MainProgress.Min := 1;
  MainProgress.Max := files.Count;
  MainProgress.Step := 1;
  // вывод результата
  ii := -1;
  for fi := 0 to (files.Count - 1) do
  begin
    SetStatus('Загрузка ' + IntToStr(fi + 1) + ' файла');
    pic := TPicture.Create;
    pic.LoadFromFile(files[fi]);
    if ((sWidth <> 0) or (sHeight <> 0)) and ((sWidth = pic.Width) and
      (sHeight = pic.Height)) then
    begin
      ii := ii + 1;
      resolution := IntToStr(pic.Width) + 'x' + IntToStr(pic.Height);
      MainProgress.Position := ii + 1;
      LtvResults.Items.Add;
      LtvResults.Items[ii].Caption := files[fi];
      LtvResults.Items[ii].SubItems.Add(resolution);
      ImLResults.Add(pic.Bitmap, nil);
      LtvResults.Items[ii].ImageIndex := ii;
    end;
    pic.Destroy;
  end;
  SetStatus('Готово! Найдено ' + IntToStr(ii+1) + ' файлов.');
  files.Free;
end;

end.
