unit about;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

    { TAboutWindow }

    TAboutWindow = class(TForm)
        BtnOK: TButton;
        Label1: TLabel;
        Label2: TLabel;
        Label3: TLabel;
        Label4: TLabel;
        Label5: TLabel;
        procedure BtnOKClick(Sender: TObject);
    private

    public

    end;

var
    AboutWindow: TAboutWindow;

implementation

{$R *.lfm}

{ TAboutWindow }

procedure TAboutWindow.BtnOKClick(Sender: TObject);
begin
    Self.DestroyWnd;
end;

end.

