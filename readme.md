# Icon Finder

Application for searching icons for a given pattern, resolution and file type.

## Requirements

* Lazarus>=2.0.6
* FPC>=3.0.4